public class HelloWorld {
    public static void main (String[] args){
        String name = "Niamh";
        String output = "Hello " + name + "!";
        System.out.println(output);


        int x = 7;

        if (x < 10){
            System.out.println("x is less than 10");
        }  else if (x <= 100){
            System.out.println("x is between 10 and 100");
        }  else
        {
            System.out.println("x is greater than 10");
        }

        String [] animals = {"cat", "dog", "tortoise", "rabbit"};

        int numDogs = 0;
        int numCats = 0;

        for (int i = 0; i < animals.length; i ++){
            if (animals[i] == "cat"){
                numCats ++;
            }
            else if (animals[i]== "dog"){
                numDogs ++;
            }
        }
        System.out.println("Number of cats: " + numCats);
        System.out.println("Number of dogs: " + numDogs);
    }
    }

}
